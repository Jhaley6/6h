import React from "react";
import {Routes, Route} from 'react-router-dom'
import Gallery from "./Routes/Gallery";
import Home from './Routes/Home'
import About from "./Routes/About";
import Contact from "./Routes/Contact";

function App() {
  return (
    <>
    <Routes>
      <Route path='/' element={<Home/>}/>
      <Route path='Gallery' element ={<Gallery/>}/>
      <Route path='About' element ={<About/>}/>
      <Route path='Contact' element ={<Contact/>}/>

    </Routes>

    </>
  );
}

export default App;
