import React from 'react'
import { Link } from 'react-router-dom'
import './LogoStyles.css'
// import CoLogo from '../Assets/6hlinelogoalone.png'

const Logos = () => {
    return (
        <div className='logo'>
            {/* <logo><source src={CoLogo} /></logo> */}
            <div className='content'>
                <h1>Specializing in Concrete Restoration Services.</h1>
                <p>Slab Jacking. Crack Sealing.</p>

                <div>
                    <Link to='Gallery' className='btn'>View Gallery</Link>
                    <Link to='About' className='btn'>Learn More</Link>
                    <Link to='Contact' className='btn btn-light'>Contact Us</Link>
                </div>
            </div>
        </div>

    )
}

export default Logos
