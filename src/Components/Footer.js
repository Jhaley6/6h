import React from 'react'
import './FooterStyles.css'
import { FaFacebook, FaInstagram, FaMailBulk, FaPhone, FaSearchLocation } from 'react-icons/fa'

const Footer = () => {
    return (
        <div className='footer'>
            <div className='footer-container'>
                <div className='left'>  </div>
                <div className='right'>
                <div classname='location'>
                        <h4><FaSearchLocation size={20} style={{ color: 'white', marginRight: '2rem' }} />Serving central Illinois</h4>
                    </div>
                    <div className='phone'>
                        <h4> <FaPhone size={20} style={{ color: 'white', marginRight: '2rem' }} />1-309-620-4409</h4>
                    </div>
                    <div className='email'>
                        <h4><FaMailBulk size={20} style={{ color: 'white', marginRight: '2rem' }} />jub.6hslabjacking@gmail.com</h4>
                        <br></br>
                        <div className='social'><a href = "https://www.facebook.com/6hslabjack"><FaFacebook size={30} style={{ color: 'white', marginRight: '1rem' }} /></a></div><br></br>
                    <div className='social'><a href = "https://www.instagram.com/6hslabjack"><FaInstagram size={30} style={{ color: 'white', marginRight: '1rem' }} /></a></div>
                    </div>
                    
                    
                    
                </div>
            </div>
        </div>
    )
}

export default Footer
