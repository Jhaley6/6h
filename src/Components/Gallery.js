import React from 'react'
import './Gallerystyles.css'
import '../index.css'
import { Link } from 'react-router-dom'
import pic1 from '../Images/beforeafter.JPG'
import pic2 from '../Images/2ndbeforeafter.JPG'
import pic3 from '../Images/working.jpg'

const Gallery = () => {
  return (
    <div className='gallery'>
        <div className='card-container'>
            <div className='card'>
                <h3> Sidewalk Fix </h3>
             
                <a ><img src = {pic1} style={{ height: '75%', width: '75%' }}/></a>
                <Link to='/Contact' className='btn'>Get a Quote</Link>
            </div>
            <div className='card'>
                <h3> Sidwalk to Driveway Fix  </h3>
                <a ><img src = {pic2} style={{ height: '75%', width: '75%' }}/></a>
                <Link to='/Contact' className='btn'>Get a Quote</Link>
            </div>
            <div className='card'>
                <h3> Sidwalk to Driveway Fix  </h3>
                <a ><img src = {pic3} style={{ height: '75%', width: '75%' }}/></a>
                <Link to='/Contact' className='btn'>Get a Quote</Link>
            </div>
        </div>
      
    </div>
  )
}

export default Gallery
