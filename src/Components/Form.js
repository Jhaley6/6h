import React from 'react'
import './Formstyles.css'

const Form = () => {
  
  return (
    <div className='form'>
        <form taget="_blank"
        action="https://formsubmit.co/24205d729cfc28c4095e51e4c4e19c84" method="POST" onsubmit="setAlert">
                <input type="hidden" name="_subject" value="Slab Jacking Message"/>
                <input type="hidden" name="_autoresponse" value="We will contact you shortly!"/>
                <input type="hidden" name="_cc" value="keein4@hotmail.com"/>
                <input type="hidden" name="_next" value="http://6h-slab-jacking.s3-website.us-east-2.amazonaws.com/"/>
            <label>Your Name</label>
            <input type="text" name = "name" placeholder="first and last name" className="form-control" required></input>
            <label>Email</label>
            <input type="email" name="email" placeholder="email address" className="form-control" required></input>
            <label>Contact Number</label>
            <input type='phone' placeholder='xxx-xxx-xxxx' className="form-control" required></input>
            <label>Information About Your Request</label>
            <textarea rows='6' placeholder='job description or question' className="form-control" required/>
            <button type = "submit" className='btn'>Submit</button>
        </form>
              
    </div>
    
  )
}

export default Form
