import React from 'react'
import {Link} from 'react-router-dom'
import './Aboutstyles.css'

const About = () => {
  return (
    <div className='about'>
        <div className='left'>
            <h1>Tell Me More</h1>
            <ol>
                <h3>What can be leveled?</h3>
                <ul> Concrete leveling can be done on sidewalks, driveways, porches, etc.</ul>
                <h3>Why raise instead of replace?</h3>
                <ul> Concrete leveling is approximately half the cost of replacing the concrete.</ul>
                <h3>What are the benefits of slabjacking?</h3>
                    <ul>*Cost effective</ul>
                    <ul>*Little to no disruption or mess left behind</ul>
                    <ul>*Immediate use </ul>
                    <ul>*Provides a strong base</ul>
                <h3>What areas do you service?</h3>
                <ul> Mackinaw, IL and surrounding areas</ul>
                <h3>Why have it done?</h3>
                <ul>Improve looks, functionality or even safety of the area.</ul>
            </ol>
            
            <Link to='/Contact'><button className='btn'>Contact</button></Link>
        </div>
        {/* <div className='right'> */}
            {/* <div className='img-container'>
                <div className='image-stack top'>
                    <img src={} className='img' alt='' />
                </div>
                <div className='image-stack bottom'>
                    <img src={} className='img' alt='' />
                </div>
            </div> */}
        {/* </div> */}
      
    </div>
  )
}

export default About
