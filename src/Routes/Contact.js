import React from 'react'
import NavBar from '../Components/NavBar'
import Logos from '../Components/Logos'
import LogoImage from '../Components/Logoimage'
import Form from '../Components/Form'



const Contact = () => {
  return (
    <div>
      <NavBar/>
      <LogoImage heading='CONTACT US.' text='Call, text, leave a message or complete the form below.'/>
      
      <Form/>
          
    </div>
  )
}

export default Contact
