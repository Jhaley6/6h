import React from 'react'
import NavBar from '../Components/NavBar'
import Footer from '../Components/Footer'
import Logoimage from '../Components/Logoimage'
import GalleryCards from '../Components/Gallery'

const Gallery = () => {
  return (
    <div>
      <NavBar/>
      <Logoimage heading='PROJECT GALLERY' text='View completed projects.'/>
      <GalleryCards/>
      <Footer/>
    </div>
  )
}

export default Gallery
