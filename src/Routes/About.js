import React from 'react'
import Footer from '../Components/Footer'
import Logoimage from '../Components/Logoimage'
import NavBar from '../Components/NavBar'
import AboutSection from '../Components/About'

const About = () => {
  return (
    <div>
      <NavBar/>
      <Logoimage heading='What is slab jacking?' text='Scroll down to learn more about the process.' />
      <AboutSection/>
      <Footer/>
    </div>
  )
}

export default About
