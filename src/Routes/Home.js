import React from 'react'
import Footer from '../Components/Footer'
import Logos from '../Components/Logos'
import NavBar from '../Components/NavBar'


const Home = () => {
  return (
    <div>
      <NavBar/>
      <Logos/>
      <Footer/>
    </div>
  )
}

export default Home
